package edu.sjsu.android.zoodirectory;

import android.os.Bundle;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;

public class ProfileActivity extends AppCompatActivity {
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity);
        ImageView img = findViewById(R.id.picture);
        TextView nameTxt = findViewById(R.id.nameTextView);
        TextView infoTxt = findViewById(R.id.infoTextView);
        String username = null;
        String info = null;
        int key = 0;
        Bundle extras = getIntent().getExtras();
        if(extras != null) {
            username = extras.getString("animal");
            info = extras.getString("info");
            key = extras.getInt("key");
        }
        if(key == 1) img.setImageResource(R.drawable.giraffe);
        else if(key == 2) img.setImageResource(R.drawable.monkey);
        else if(key == 3) img.setImageResource(R.drawable.elephant);
        else if(key == 4) img.setImageResource(R.drawable.penguin);
        else if(key == 5) img.setImageResource(R.drawable.panda);
        nameTxt.setText(username);
        infoTxt.setText(info);
    }
}
