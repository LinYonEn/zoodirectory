package edu.sjsu.android.zoodirectory;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.net.Uri;
import android.view.View;
import android.view.View.OnClickListener;

import android.os.Bundle;
import android.widget.Button;

public class info_activity extends AppCompatActivity {

    final String phoneNumber = "tel:8888888";
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_info);
        Button callButton = (Button) findViewById(R.id.call);
        callButton.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View view) {
                Uri number = Uri.parse(phoneNumber);
                Intent callIntent = new Intent(Intent.ACTION_DIAL, number);
                startActivity(callIntent);
                finish();
            }
        });
    }
}