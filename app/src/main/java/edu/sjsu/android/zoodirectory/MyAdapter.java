package edu.sjsu.android.zoodirectory;

import java.util.List;
import androidx.recyclerview.widget.RecyclerView;

import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

public class MyAdapter extends RecyclerView.Adapter<MyAdapter.ViewHolder> {
    private List<String> values;
    private RecyclerViewClickListener listen;



    //Provide a reference to the view for each data item
    //Complex data items may need more than one view per item, and
    //you provide access to all the views for a data item in a view holder
    public class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
        //each data item is just a string in this case
        public TextView txtHeader;
        public TextView txtFooter;
        public View layout;
        public ImageView image;

        public ViewHolder(View v) {
            super(v);
            layout = v;
            txtHeader = (TextView) v.findViewById(R.id.firstLine);
            txtFooter = (TextView) v.findViewById(R.id.secondLine);
            image = (ImageView) v.findViewById(R.id.icon);
            v.setOnClickListener(this);
        }

        @Override
        public void onClick(View view) {
            listen.onClick(view, getAdapterPosition());
        }
    }
    public void add (int position, String item) {
        values.add(position, item);
        notifyItemInserted(position);

    }

    public void remove (int position) {
        values.remove(position);
        notifyItemRemoved(position);
    }

    //Provide a suitable constructor (depends on the kind of dataset)
    public MyAdapter(List<String> myDataset, RecyclerViewClickListener listener) {
        values = myDataset;
        this.listen = listener;
    }
    //Create new view (invoked by the layout manager)
    @Override
    public MyAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        //create a new view
        LayoutInflater inflater = LayoutInflater.from(parent.getContext());
        View v = inflater.inflate(R.layout.row_layout, parent, false);
        //set the view's size, margins, paddings and layout parameters
        ViewHolder vh = new ViewHolder(v);
        return vh;
    }
    //Replace the contents of a view (invoked by the layout manager)
    @Override
    public void onBindViewHolder(ViewHolder holder, final int position) {
        // - get element from your dataset at this position
        // - replace the contents of the view with that element
        final String name = values.get(position);
        if(position == 0) holder.image.setImageResource(R.drawable.giraffe);
        else if(position == 1) holder.image.setImageResource(R.drawable.monkey);
        else if(position == 2) holder.image.setImageResource(R.drawable.elephant);
        else if(position == 3) holder.image.setImageResource(R.drawable.penguin);
        else if(position == 4) holder.image.setImageResource(R.drawable.panda);
        holder.txtHeader.setText(name);
        holder.txtHeader.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                remove(position);
            }
        });
        holder.txtFooter.setText(name);
    }

    //Return the size of your dataset (invoked by the layout manager)
    @Override
    public int getItemCount() {
        return values.size();
    }

    public interface RecyclerViewClickListener{
        void onClick(View v, int position);
    }

}
