package edu.sjsu.android.zoodirectory;

import android.app.Activity;
import android.content.Intent;
import android.graphics.drawable.Icon;
import android.net.Uri;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.ItemTouchHelper;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import java.util.ArrayList;
import java.util.List;

import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;

import edu.sjsu.android.zoodirectory.R;

public class MainActivity extends AppCompatActivity {
    private RecyclerView recyclerView;
    private RecyclerView.Adapter mAdapter;
    private RecyclerView.LayoutManager layoutManager;
    private ImageView imageChange;
    private MyAdapter.RecyclerViewClickListener listener;
    List<String> input = new ArrayList<>();
    @Override

    protected void onCreate(Bundle savedInstanceState) {
        setOnClickListener();
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        recyclerView = (RecyclerView) findViewById(R.id.my_recycler_view);
        imageChange = (ImageView) findViewById(R.id.icon);
        //use this setting to import performance if you know that changes
        //in content do not change the layout size of the RecyclerView
        recyclerView.setHasFixedSize(true);
        //use a linear layout manager
        layoutManager = new LinearLayoutManager(this);
        recyclerView.setLayoutManager(layoutManager);
        input.add(0, "Giraffe");
        input.add(1, "Monkey");
        input.add(2, "Elephant");
        input.add(3, "Penguin");
        input.add(4, "Panda");
        mAdapter = new MyAdapter(input, listener);
        recyclerView.setAdapter(mAdapter);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater menuInflater = getMenuInflater();
        menuInflater.inflate(R.menu.main_activity_menu, menu);
        return super.onCreateOptionsMenu(menu);
    }

    private void setOnClickListener() {
        listener = new MyAdapter.RecyclerViewClickListener() {
            @Override
            public void onClick(View v, int position) {
                Intent intent = new Intent(getApplicationContext(), ProfileActivity.class);
                if(position == 0) {
                    intent.putExtra("animal", "Giraffe");
                    intent.putExtra("info", "The giraffe is an African artiodactyl mammal, the tallest living terrestrial animal and the largest ruminant. It is traditionally considered to be one species, Giraffa camelopardalis, with nine subspecies.");
                    intent.putExtra("key", 1);
                }
                else if(position == 1) {
                    intent.putExtra("animal", "Monkey");
                    intent.putExtra("info", "Monkey is a common name that may refer to most mammals of the infraorder Simiiformes, also known as the simians.");
                    intent.putExtra("key", 2);
                }
                else if(position == 2) {
                    intent.putExtra("animal", "Elephant");
                    intent.putExtra("info", "Elephants are the largest existing land animals. Three living species are currently recognised: the African bush elephant, the African forest elephant, and the Asian elephant. They are an informal grouping within the family Elephantidae of the order Proboscidea.");
                    intent.putExtra("key", 3);
                }
                else if(position == 3) {
                    intent.putExtra("animal", "Penguin");
                    intent.putExtra("info", "Penguins are a group of aquatic flightless birds. They live almost exclusively in the southern hemisphere: only one species, the Galapagos penguin, is found north of the Equator. Highly adapted for the life in the water, penguins have countershaded dark and white plumage and flippers for swimming.");
                    intent.putExtra("key", 4);
                }
                else if(position == 4){
                    intent.putExtra("animal", "Giant Panda");
                    intent.putExtra("info", "The giant panda, also known as the panda bear, is a bear native to South Central China. It is characterised by its bold black-and-white coat and rotund body. The name \"giant panda\" is sometimes used to distinguish it from the red panda, a neighboring musteloid.");
                    intent.putExtra("key", 5);
                }
                startActivity(intent);
            }
        };
    }

    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {
        switch (item.getItemId()) {
            case R.id.info_item:
                startActivity(new Intent(MainActivity.this, info_activity.class));
            case R.id.delete_item:
                Intent uninstallIntent = new Intent(Intent.ACTION_DELETE);
                startActivity(uninstallIntent);
        }
        return super.onOptionsItemSelected(item);
    }
}